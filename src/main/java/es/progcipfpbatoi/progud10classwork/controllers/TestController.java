package es.progcipfpbatoi.progud10classwork.controllers;

import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {
	
	@ResponseBody
	@GetMapping("/test")
	public String getTest() {
		Random random = new Random();
		int numero = random.nextInt(101);
		
		return "<html>"
				+ "<body>"
				+ "<h1> Bola: " + numero + " </h1>"
				+ "</body>"
				+ "</html>";
	}

}

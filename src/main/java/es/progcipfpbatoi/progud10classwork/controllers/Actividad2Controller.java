package es.progcipfpbatoi.progud10classwork.controllers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Actividad2Controller {

	@ResponseBody
	@GetMapping("/fecha-actual-es")
	public String getFechaActualES() {
		LocalDateTime ahora = LocalDateTime.now();
		DateTimeFormatter f = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		return ahora.format(f);
	}
}

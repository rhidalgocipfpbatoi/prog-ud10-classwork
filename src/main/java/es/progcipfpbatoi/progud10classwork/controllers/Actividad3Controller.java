package es.progcipfpbatoi.progud10classwork.controllers;

import java.util.HashMap;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Actividad3Controller {

	@GetMapping("/calculadora")
	public String calculadoraAction() {
		return "calculadora_view";
	}
	
	@GetMapping("/suma")
	@ResponseBody
	public String sumar(@RequestParam int sumando1,
						@RequestParam int sumando2) {
		return String.format("%d + %d = %d", sumando1, sumando2, 
				(sumando1 + sumando2));
	}
	
	@GetMapping("/resta")
    @ResponseBody
    public String restar(@RequestParam int operando1,
                             @RequestParam int operando2) {
        return String.format(
        		"%d - %d = %d"
        		+ "<br>"
        		+ "%d - %d = %d", 
        		operando1, operando2, (operando1 - operando2),
        		operando2, operando1, (operando2 - operando1));
    }
	
	@GetMapping("/multiplica")
    @ResponseBody
    public String multiplicar(@RequestParam HashMap<String, String> params) {
        String multiplicador = params.get("multiplicador");
        String multiplicando = params.get("multiplicando");
        
        if (multiplicador != null && multiplicando != null) {
            try {
                int multiplicadorNumeric = Integer.parseInt(multiplicador);
                int multiplicandoNumeric = Integer.parseInt(multiplicando);
                
                return String.format("%d * %d = %d",
                        multiplicadorNumeric, multiplicandoNumeric,
                        (multiplicadorNumeric*multiplicandoNumeric));
            } catch (NumberFormatException e) {
                return "Los 2 operandos deben ser enteros";
            }
        } else {
            return "Debe indicar los 2 operandos";
        }
    }



}

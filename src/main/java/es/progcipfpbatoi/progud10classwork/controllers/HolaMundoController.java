package es.progcipfpbatoi.progud10classwork.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HolaMundoController {

	@ResponseBody
	@GetMapping("/saluda")
	public String getHolaMundo() {
		return "Hola mundo";
	}
}

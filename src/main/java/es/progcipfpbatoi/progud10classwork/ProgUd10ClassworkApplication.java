package es.progcipfpbatoi.progud10classwork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgUd10ClassworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProgUd10ClassworkApplication.class, args);
	}

}
